﻿namespace BSF.BaseService.NScript
{
    partial class FrDebug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbCompilerMode = new System.Windows.Forms.ComboBox();
            this.tbClassPath = new System.Windows.Forms.TextBox();
            this.tbMethodName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCall = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "编译方式";
            // 
            // cbCompilerMode
            // 
            this.cbCompilerMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCompilerMode.FormattingEnabled = true;
            this.cbCompilerMode.Items.AddRange(new object[] {
            "Assembly",
            "AppDomian",
            "Main"});
            this.cbCompilerMode.Location = new System.Drawing.Point(81, 18);
            this.cbCompilerMode.Name = "cbCompilerMode";
            this.cbCompilerMode.Size = new System.Drawing.Size(264, 20);
            this.cbCompilerMode.TabIndex = 1;
            // 
            // tbClassPath
            // 
            this.tbClassPath.Location = new System.Drawing.Point(81, 59);
            this.tbClassPath.Name = "tbClassPath";
            this.tbClassPath.Size = new System.Drawing.Size(264, 21);
            this.tbClassPath.TabIndex = 2;
            // 
            // tbMethodName
            // 
            this.tbMethodName.Location = new System.Drawing.Point(81, 108);
            this.tbMethodName.Name = "tbMethodName";
            this.tbMethodName.Size = new System.Drawing.Size(264, 21);
            this.tbMethodName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "类路径";
            // 
            // btnCall
            // 
            this.btnCall.Location = new System.Drawing.Point(141, 163);
            this.btnCall.Name = "btnCall";
            this.btnCall.Size = new System.Drawing.Size(75, 23);
            this.btnCall.TabIndex = 3;
            this.btnCall.Text = "模拟调用";
            this.btnCall.UseVisualStyleBackColor = true;
            this.btnCall.Click += new System.EventHandler(this.btnCall_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "方法名";
            // 
            // label3
            // 
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(9, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(336, 55);
            this.label3.TabIndex = 4;
            this.label3.Text = "程序集和应用程序域模式: 仅支持无参数方法调试;\r\nMain模式: 则无需输入类路径和方法名，仅支持无参数Main入口调试；\r\n";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(79, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "格式:NameSpace.ClassName ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(79, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "格式:MethodName";
            // 
            // FrDebug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 247);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCall);
            this.Controls.Add(this.tbMethodName);
            this.Controls.Add(this.tbClassPath);
            this.Controls.Add(this.cbCompilerMode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrDebug";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "调试界面";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrDebug_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbCompilerMode;
        private System.Windows.Forms.TextBox tbClassPath;
        private System.Windows.Forms.TextBox tbMethodName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCall;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
    }
}