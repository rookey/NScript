﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BSF.BaseService.NScript.Core
{
    public class AssemblyHelper
    {
        public static void RegisterAssemblyFind()
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {

            var find = (from o in AppDomain.CurrentDomain.GetAssemblies() where o.GetName().FullName == args.Name select o).FirstOrDefault();
            if (find != null)
                return find;
            return null;
        }
    }
}
